using HouseApp.BL.Domain;

namespace HouseApp.DAL;

public class InMemoryRepository : IRepository
{
    private readonly List<House> _houses;
    private readonly List<Realtor> _realtors;

    public InMemoryRepository()
    {
        _houses = new List<House>();
        _realtors = new List<Realtor>();

        Seed();
    }

    public void CreateHouse(House house)
    {
        house.Id = _houses.Count + 1;
        _houses.Add(house);
    }

    public IEnumerable<House> ReadHousesByMinimumArea(int mininumArea)
    {
        /*
        ICollection<House> result = new List<House>();
        foreach (var house in _houses)
        {
            if (house.SurfaceAreaSqMeters >= mininumArea)
            {
                result.Add(house);
            }
        }
        return result;
        */
        return _houses.FindAll(house =>
            house.SurfaceAreaSqMeters >= mininumArea);
    }

    /*private bool FilterHouse(House house)
    {
        return house.SurfaceAreaSqMeters >= 120;
    }*/

    private void Seed()
    {
        var house1 = new House
        {
            Address = "Pothoekstraat 125",
            SurfaceAreaSqMeters = 5000,
            HasGarden = false
        };

        var house2 = new House
        {
            Address = "Meir 1",
            SurfaceAreaSqMeters = 120,
            HasGarden = true
        };

        CreateHouse(house1);
        CreateHouse(house2);

        var realtor1 = new Realtor
        {
            Name = "Hans Immo",
            Houses = new List<House>()
        };

        var realtor2 = new Realtor
        {
            Name = "Heylen Vastgoed",
            Houses = new List<House>()
        };

        _realtors.Add(realtor1);
        _realtors.Add(realtor2);

        realtor1.Houses.Add(house1);
        realtor2.Houses.Add(house2);

        house1.Realtor = realtor1;
        house2.Realtor = realtor2;
    }
}
