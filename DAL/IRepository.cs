using HouseApp.BL.Domain;

namespace HouseApp.DAL;

public interface IRepository
{
    void CreateHouse(House house);
    IEnumerable<House> ReadHousesByMinimumArea(int mininumArea);
}
