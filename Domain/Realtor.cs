namespace HouseApp.BL.Domain;

public class Realtor
{
    public long Id { get; set; }
    public string Name { get; set; }

    public ICollection<House> Houses { get; set; }

    public override string ToString()
    {
        var houseCount = Houses == null ? 0 : Houses.Count;
        return $"REALTOR: {Name,20} {houseCount} houses"; // TODO
    }
}
