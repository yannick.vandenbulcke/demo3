namespace HouseApp.BL.Domain;

public class House
{
    public long Id { get; set; }
    public int SurfaceAreaSqMeters { get; set; }
    public bool HasGarden { get; set; }
    public DateOnly BuildDate { get; set; }
    public int NrOfFloors { get; set; }
    public string Address { get; set; }

    public Realtor Realtor { get; set; }
}
