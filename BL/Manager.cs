using HouseApp.BL.Domain;
using HouseApp.DAL;

namespace HouseApp.BL;

public class Manager : IManager
{
    private readonly IRepository _repository;

    public Manager(IRepository repository)
    {
        _repository = repository;
    }

    public House AddHouse(string address, bool hasGarden, int surfaceAreaSqMeters)
    {
        var house = new House
        {
            Address = address,
            HasGarden = hasGarden,
            SurfaceAreaSqMeters = surfaceAreaSqMeters
        };
        _repository.CreateHouse(house);
        return house;
    }

    public IEnumerable<House> GetHousesByMinimumArea(int mininumArea)
    {
        return _repository.ReadHousesByMinimumArea(mininumArea);
    }
}
