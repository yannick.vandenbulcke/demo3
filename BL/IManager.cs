using HouseApp.BL.Domain;

namespace HouseApp.BL;

public interface IManager
{
    House AddHouse(string address, bool hasGarden, int surfaceAreaSqMeters);
    IEnumerable<House> GetHousesByMinimumArea(int mininumArea);
}
