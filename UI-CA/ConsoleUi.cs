using HouseApp.BL;
using HouseApp.BL.Domain;
using HouseApp.UI.CA.Extensions;

namespace HouseApp.UI.CA;

public class ConsoleUi
{
    private readonly IManager _manager;

    public ConsoleUi(IManager manager)
    {
        _manager = manager;
    }

    public void Run()
    {
        AddNewHouse(); // TODO: REMOVE ME!!!
        int choice;
        do
        {
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("0) Afsluiten");
            Console.WriteLine("1) Zoek huizen met een minimum oppervlakte");

            choice = Int32.Parse(Console.ReadLine());
            if (choice < 0 || choice > 1)
            {
                Console.WriteLine("Gelieve met 0 of 1 te antwoorden!");
            }
            else if (choice == 1)
            {
                SearchHousesBySurfaceArea();
            }
        } while (choice != 0);
    }

    private void AddNewHouse()
    {
        // TODO: ask for input!!!

        _manager.AddHouse("Nationalestraat 5", false, 8000);
    }

    private void SearchHousesBySurfaceArea()
    {
        Console.Write("Minimum aantal m²? ");
        string input = Console.ReadLine();
        int area;
        bool success = Int32.TryParse(input, out area);
        while (!success || area < 0)
        {
            Console.Write("Minimum aantal m²? ");
            input = Console.ReadLine();
            success = Int32.TryParse(input, out area);
        }

        var searchResult = _manager.GetHousesByMinimumArea(area);
        List<House> houses = new List<House>(searchResult);
        houses.ForEach(house => Console.WriteLine(house.StringRepresentation()));
        /*
        foreach (var house in searchResult)
        {
            Console.WriteLine(house);
        }
        */
        
        // Maybe later
        // var houseStrings = houses.Select(house => house.ToString());
    }
}
