using HouseApp.BL.Domain;

namespace HouseApp.UI.CA.Extensions;

public static class HouseExtensions
{
    public static string StringRepresentation(this House house)
    {
        var text = $"{house.Address,30} - {house.SurfaceAreaSqMeters:###}m²";
        if (house.HasGarden)
        {
            text += " WITH garden";
        }
        return text;
    }
}
