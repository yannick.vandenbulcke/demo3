namespace HouseApp.UI.CA.Extensions;

// USAGE:
// DateTime today = DateTime.Today;
// today.Print(false);
public static class DateTimeExtensions
{
    public static void Print(this DateTime dateTime, bool prettyFormat)
    {
        if (prettyFormat)
        {
            Console.WriteLine($"{dateTime:yyyy MMMM dd}");
        }
        else
        {
            Console.WriteLine($"{dateTime:yyyy-MM-dd}");
        }
    }
}
