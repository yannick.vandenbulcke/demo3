﻿using HouseApp.BL;
using HouseApp.DAL;
using HouseApp.UI.CA;

var repository = new InMemoryRepository();
var manager = new Manager(repository);

var ui = new ConsoleUi(manager);
ui.Run();
